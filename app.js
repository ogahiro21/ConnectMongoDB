var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var model = require('./model.js');
var Users = model.Users;
var mongoose = require('mongoose');

var routes = require('./routes/index');

// ---- Renamed and Added by tdomen -----
var login = require('./routes/login');
var auth = require('./routes/auth/login');
var http = require('http');
var mongoose = require('mongoose');
// --------------------------------------

var app = express();


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


var router = express.Router();

// app.get('/login', function(req, res) {
//   res.render('login');
// });

// ---- Added by tdomen -----
// ポート設定
// app.set('port', process.env.PORT || 3000);
// --------------------------

app.use('/login', login);
app.use('/', routes);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

// サーバ立ち上げ
http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
    mongoose.connect('mongodb://localhost:27017/backend', {useNewUrlParser: true });
});

module.exports = app;
