# Connect MongoDB
HTMLから受け取った情報をMongoDBへ保存する。

## OverView
expressを用いてルーティングを行い、mongooseを使ってDBへ保存する。

## Requirement
- Ubuntu 18.06 LTS
- npm 5.6.0
- node.js 8.11.3
- express.js 4.0.0
- MongoDB 4.0.1

## Usage
### Create collection
```
$ mongo
> use backend
> db.createCollection('users')
> show collections
```
### Launch MongoDB
```
$ sudo service mongod start
```
### Starting the server
```
$ npm start
```
### Access to localhost
[http://localhost:3000/login](http://localhost:3000/login)
### Confirm input data
```
$ mongo
$ use backend
$ db.users.find()
```

## Reference
Express 公式ドキュメント - [Express でのルーティング](http://expressjs.com/ja/guide/routing.html)
