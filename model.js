var mongoose = require('mongoose');
// var Schema = mongoose.Schema;

mongoose.connect("mongodb://localhost:27017/sample", { useNewUrlParser: true });

var userScheme = mongoose.Schema({
  id: String,
  email: String,
  name: String,
  password: String,
  role: String
});

exports.Users = mongoose.model('Users', userScheme);
